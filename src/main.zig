const std = @import("std");
const yazap = @import("yazap");
const ac = @import("autocomplete.zig");

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var app = yazap.App.init(
        allocator,
        "",
        "Example autocomple command",
    );
    defer app.deinit();
    {
        var root_cmd = app.rootCommand();
        {
            try root_cmd.addArgs(@constCast(&[_]yazap.Arg{
                yazap.Arg.booleanOption(
                    "version",
                    'v',
                    "print version and exit",
                ),
            }));
            {
                var subcommand = app.createCommand("subcommand", "example subcommand");
                subcommand.setProperty(.positional_arg_required);
                try subcommand.addArgs(@constCast(&[_]yazap.Arg{
                    yazap.Arg.positional(
                        "FILE",
                        null,
                        null,
                    ),
                    yazap.Arg.singleValueOption(
                        "yolo",
                        'y',
                        "only once",
                    ),
                }));
                try root_cmd.addSubcommand(subcommand);
            }
        }
    }
    ac.autocomplete_exit(&app);

    const matches = try app.parseProcess();
    if (matches.subcommandMatches("subcommand")) |sc| {
        std.log.info(
            "match: {s}",
            .{sc.getSingleValue("FILE").?},
        );        
    }

}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
