#/usr/bin/env bash

_zig_autocomplete_v1()
{
    local cur prev words cword
    _init_completion -n = || return

    local ARGS=("$@")

    debug_print() {
        printf "$@" 1>&2      
    }

    debug_print "\n"
    debug_print "parse_help:%s\n" "$(_parse_help "$1")"
    debug_print "cur: %s\n" "$cur"
    debug_print "prev: %s\n" "$prev"
    debug_print "words: %s\n" "$words"
    debug_print "cword: %s\n" "$cword"
    # if [ "$(readlink -f "${ARGS[0]}")" != "$COM" ]; then
    #     debug_print "NOPE\n"
    #     return
    # fi

    if [ ! -x "${ARGS[0]}" ]; then
        debug_print "NOPENOPE\n"
        return
    fi
    if ! grep -q __ZIG_COMPLETE_SAFETY_SYMBOL "${ARGS[0]}"; then
        debug_print "DEFAULT"
        return compgen "${ARGS[@]}" -o default
    fi

    debug_print "WORDS: "
    for i in "${!COMP_WORDS[@]}"; do 
        debug_print "%s:%s\t" "$i" "${COMP_WORDS[$i]}"
    done
    debug_print "\n"

    (
        set -e
        local COM_REPLY="$(__ZIG_COMPLETE_RUN=run "${ARGS[0]}" "${COMP_WORDS[@]:1}")"
        debug_print "COM_REPLY: %s\n" "$COM_REPLY"

        # COMPREPLY=($(compgen -W "now tomorrow never" "${COMP_WORDS[-1]}"))
        # debug_print "REPLY: "
        # for i in "${!COMPREPLY[@]}"; do 
        #     debug_print "%s:%s\t" "$i" "${COMPREPLY[$i]}"
        # done
        echo
    )
} && 