const std = @import("std");
const yazap = @import("yazap");

pub fn autocomplete_exit(app: *yazap.App) void {
    if (autocomplete(app) catch @panic("autocomplete raised an error")) {
        std.process.exit(0);
    }
}

pub fn autocomplete(app: *yazap.App) !bool {
    const env = try std.process.getEnvMap(app.allocator);
    const stdout = std.io.getStdOut();
    var args = try std.process.argsAlloc(app.allocator);
    defer app.allocator.free(args);
    const com = args[0];
    app.command.name = std.fs.path.basename(com);

    if (env.get("__ZIG_COMPLETE_RUN")) |zig_complete| {
        if (std.mem.eql(u8, zig_complete, "eval")) {
            try eval_autocomplete(com);
        } else {
            var command = app.command;
            var skip_next: usize = 0;
            outer: for (args[1..], 0..) |arg, arg_i| {
                if (skip_next > 0) {
                    skip_next -= 1;
                    continue;
                }
                const args_left = args.len - arg_i;
                _ = args_left; // autofix
                for (command.subcommands.items) |sc| {
                    if (std.mem.eql(u8, sc.name, arg)) {
                        command = sc;
                        continue :outer;
                    }
                }
                for (command.options.items) |opt| {
                    if (opt.long_name) |ln| {
                        if (std.mem.eql(u8, ln, arg)) {
                            continue :outer;
                        }
                    }
                }
                _ = try stdout.write(arg);
                _ = try stdout.write("\n");
            }
        }
        return true;
    }
    return false;
}

pub fn eval_autocomplete(prog: []const u8) !void {
    const complete_sh = @embedFile("autocomplete.tpl.sh");
    const stdout = std.io.getStdOut();
    _ = try stdout.write(complete_sh);
    try std.fmt.format(
        stdout.writer(),
        "complete -F _zig_autocomplete_v1 '{s}'\n\n",
        .{std.fs.path.basename(prog)},
    );
}
