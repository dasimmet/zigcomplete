# Zig bash autocompletion

this is a WIP extension to zig's `yazap` argument parser
embedding bash autocompletion inside the binary.

```sh
eval "$(__ZIG_COMPLETE_RUN=eval zig build run --)"

./zig-out/bin/zigcomplete <tab> <tab>

```